"""initial

Revision ID: 2200b2d302ca
Revises: 
Create Date: 2023-05-13 00:52:52.285292

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2200b2d302ca'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    pass


def downgrade() -> None:
    pass
